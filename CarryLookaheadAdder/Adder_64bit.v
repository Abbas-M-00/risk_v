module FullAdder (Cin, x, y, s, Cout);
	input Cin, x, y;
	output s, Cout;
	assign s = x^y^Cin, Cout = (x & y) | (x & Cin) | (y & Cin);
endmodule

module CarryLookaheadAdder_64bit(carryin, X, Y, S, carryout);
	
	input carryin;
	input [7:0] X, Y;

	output [7:0] S;
	output carryout;
	wire [7:0] C;
	wire [7:0] G, P;

	assign C[0] = carryin;
	
        assign G[0] = X[0] & Y[0];
        assign G[1] = X[1] & Y[1];
        assign G[2] = X[2] & Y[2];
        assign G[3] = X[3] & Y[3];
        assign G[4] = X[4] & Y[4];
        assign G[5] = X[5] & Y[5];
        assign G[6] = X[6] & Y[6];
        assign G[7] = X[7] & Y[7];
        assign P[0] = X[0] | Y[0];
        assign P[1] = X[1] | Y[1];
        assign P[2] = X[2] | Y[2];
        assign P[3] = X[3] | Y[3];
        assign P[4] = X[4] | Y[4];
        assign P[5] = X[5] | Y[5];
        assign P[6] = X[6] | Y[6];
        assign P[7] = X[7] | Y[7];
        assign C[1] = G[0] | P[0]&C[0];
        assign C[2] = G[1] | P[1]&G[0] | P[1]&P[0]&C[0];
        assign C[3] = G[2] | P[2]&G[1] | P[2]&P[1]&G[0] | P[2]&P[1]&P[0]&C[0];
        assign C[4] = G[3] | P[3]&G[2] | P[3]&P[2]&G[1] | P[3]&P[2]&P[1]&G[0] | P[3]&P[2]&P[1]&P[0]&C[0];
        assign C[5] = G[4] | P[4]&G[3] | P[4]&P[3]&G[2] | P[4]&P[3]&P[2]&G[1] | P[4]&P[3]&P[2]&P[1]&G[0] | P[4]&P[3]&P[2]&P[1]&P[0]&C[0];
        assign C[6] = G[5] | P[5]&G[4] | P[5]&P[4]&G[3] | P[5]&P[4]&P[3]&G[2] | P[5]&P[4]&P[3]&P[2]&G[1] | P[5]&P[4]&P[3]&P[2]&P[1]&G[0] | P[5]&P[4]&P[3]&P[2]&P[1]&P[0]&C[0];
        assign C[7] = G[6] | P[6]&G[5] | P[6]&P[5]&G[4] | P[6]&P[5]&P[4]&G[3] | P[6]&P[5]&P[4]&P[3]&G[2] | P[6]&P[5]&P[4]&P[3]&P[2]&G[1] | P[6]&P[5]&P[4]&P[3]&P[2]&P[1]&G[0] | P[6]&P[5]&P[4]&P[3]&P[2]&P[1]&P[0]&C[0];
        assign S[0] = X[0] ^ Y[0] ^ C[0];
        assign S[1] = X[1] ^ Y[1] ^ C[1];
        assign S[2] = X[2] ^ Y[2] ^ C[2];
        assign S[3] = X[3] ^ Y[3] ^ C[3];
        assign S[4] = X[4] ^ Y[4] ^ C[4];
        assign S[5] = X[5] ^ Y[5] ^ C[5];
        assign S[6] = X[6] ^ Y[6] ^ C[6];
        assign S[7] = X[7] ^ Y[7] ^ C[7];

endmodule
